package com.converter.numberconverter.controller;

import com.converter.numberconverter.enums.NumberConversionType;
import com.converter.numberconverter.data.NumberConverterData;
import com.converter.numberconverter.service.NumberConverterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.Optional;

@RestController
public class NumberConverterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberConverterController.class);

    private final NumberConverterService converterService;

    @Autowired
    public NumberConverterController(NumberConverterService converterService) {
        this.converterService = converterService;
    }

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NumberConverterData> convertNumber(@RequestParam NumberConversionType convertType,
                                                             @RequestParam String number) {
        try {
            Optional<String> convertedNumber = converterService.convertNumber(number, convertType);
            if (convertedNumber.isPresent()) {
                return new ResponseEntity<>(new NumberConverterData().withResult(convertedNumber.get(), convertType), HttpStatus.OK);
            }
            String message = MessageFormat.format("Invalid request data for parameters: {0} and {1}!", number, convertType);
            LOGGER.error(message);
            return new ResponseEntity<>(new NumberConverterData().withMessage(message), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            String message = MessageFormat.format("Converting for parameters: {0} and {1} not possible!", number, convertType);
            LOGGER.error(message, e);
            return new ResponseEntity<>(new NumberConverterData().withMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
