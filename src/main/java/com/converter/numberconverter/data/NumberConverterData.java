package com.converter.numberconverter.data;

import com.converter.numberconverter.enums.NumberConversionType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NumberConverterData {

    private String convertedNumber;
    private NumberConversionType type;
    private String message;

    public NumberConverterData withResult(String convertedNumber, NumberConversionType type) {
        this.convertedNumber = convertedNumber;
        this.type = type;
        return this;
    }

    public NumberConverterData withMessage(String message) {
        this.message = message;
        return this;
    }

    public String getConvertedNumber() {
        return convertedNumber;
    }

    public NumberConversionType getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
