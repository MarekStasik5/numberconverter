package com.converter.numberconverter.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class HexadecimalNumberConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(HexadecimalNumberConverter.class);

    private HexadecimalNumberConverter() {
    }

    public static Optional<String> toHexadecimal(int number) {
        if (!isNumberApplicable(number)) {
            LOGGER.error("Number: {} not correct!", number);
            return Optional.empty();
        }
        return Optional.of(Integer.toHexString(number).toUpperCase());
    }

    private static boolean isNumberApplicable(int number) {
        return number >= 0;
    }
}
