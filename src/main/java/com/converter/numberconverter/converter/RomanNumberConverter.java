package com.converter.numberconverter.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TreeMap;

public class RomanNumberConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RomanNumberConverter.class);
    public static final int MIN_VALUE = 1;
    public static final int MAX_VALUE = 3999;

    private static final TreeMap<Integer, String> map = new TreeMap<>();

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    }

    private RomanNumberConverter() {
    }

    public static String toRoman(int number) {
        if (!isNumberInRange(number)) {
            LOGGER.error("Number: {} not in range!", number);
            return null;
        }

        int l = map.floorKey(number);
        if (number == l) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number - l);
    }

    private static boolean isNumberInRange(int number) {
        return number >= MIN_VALUE && number <= MAX_VALUE;
    }
}
