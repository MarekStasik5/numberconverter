package com.converter.numberconverter.converter;

import com.converter.numberconverter.enums.NumberConversionType;
import org.springframework.core.convert.converter.Converter;

public class StringToEnumConverter implements Converter<String, NumberConversionType> {

    @Override
    public NumberConversionType convert(String source) {
        return NumberConversionType.valueOf(source.toUpperCase());
    }
}
