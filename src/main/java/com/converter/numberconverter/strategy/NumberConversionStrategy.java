package com.converter.numberconverter.strategy;

import com.converter.numberconverter.enums.NumberConversionType;

import java.util.Optional;

public interface NumberConversionStrategy {

    boolean isConversionApplicable(NumberConversionType convertType);

    Optional<String> getFormattedNumber(int number);
}
