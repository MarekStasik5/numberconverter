package com.converter.numberconverter.strategy;

import com.converter.numberconverter.converter.HexadecimalNumberConverter;
import com.converter.numberconverter.enums.NumberConversionType;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class HexadecimalNumberConversionStrategy implements NumberConversionStrategy {

    @Override
    public boolean isConversionApplicable(NumberConversionType convertType) {
        return NumberConversionType.HEXADECIMAL.equals(convertType);
    }

    @Override
    public Optional<String> getFormattedNumber(int number) {
        return HexadecimalNumberConverter.toHexadecimal(number);
    }
}
