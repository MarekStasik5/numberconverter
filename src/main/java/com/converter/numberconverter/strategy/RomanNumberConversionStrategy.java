package com.converter.numberconverter.strategy;

import com.converter.numberconverter.converter.RomanNumberConverter;
import com.converter.numberconverter.enums.NumberConversionType;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RomanNumberConversionStrategy implements NumberConversionStrategy {

    @Override
    public boolean isConversionApplicable(NumberConversionType convertType) {
        return NumberConversionType.ROMAN.equals(convertType);
    }

    @Override
    public Optional<String> getFormattedNumber(int number) {
        return Optional.ofNullable(RomanNumberConverter.toRoman(number));
    }
}
