package com.converter.numberconverter.enums;

public enum NumberConversionType {
    ROMAN,
    HEXADECIMAL;
}
