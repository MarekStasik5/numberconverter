package com.converter.numberconverter.service;

import com.converter.numberconverter.enums.NumberConversionType;
import com.converter.numberconverter.strategy.NumberConversionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NumberConverterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberConverterService.class);
    private final List<NumberConversionStrategy> numberConversions;

    @Autowired
    public NumberConverterService(List<NumberConversionStrategy> numberConversions) {
        this.numberConversions = numberConversions;
    }

    public Optional<String> convertNumber(String providedNumber, NumberConversionType convertType) {
        int number = parseNumberToInt(providedNumber);
        return Optional.of(getConversionStrategy(convertType))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(strategy -> strategy.getFormattedNumber(number));
    }

    private Optional<NumberConversionStrategy> getConversionStrategy(NumberConversionType convertType) {
        return numberConversions.stream()
                .filter(v -> v.isConversionApplicable(convertType))
                .findFirst();
    }

    private int parseNumberToInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            LOGGER.error("Parsing string: {} to number not possible!", number, e);
            return 0;
        }
    }
}