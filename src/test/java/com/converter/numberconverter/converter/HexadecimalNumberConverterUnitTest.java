package com.converter.numberconverter.converter;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class HexadecimalNumberConverterUnitTest {

    @Test
    public void shouldReturnHexadecimalNumbers() {
        assertEquals(Optional.empty(), HexadecimalNumberConverter.toHexadecimal(-1));
        assertEquals(Optional.of("0"), HexadecimalNumberConverter.toHexadecimal(0));
        assertEquals(Optional.of("14"), HexadecimalNumberConverter.toHexadecimal(20));
        assertEquals(Optional.of("7FFFFFFF"), HexadecimalNumberConverter.toHexadecimal(2147483647));
    }
}
