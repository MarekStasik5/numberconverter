package com.converter.numberconverter.converter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class RomanNumberConverterUnitTest {

    @Test
    public void shouldReturnRomanNumbers() {
        assertEquals("II", RomanNumberConverter.toRoman(2));
        assertEquals("DCCLII", RomanNumberConverter.toRoman(752));
        assertEquals("MVII", RomanNumberConverter.toRoman(1007));
    }

    @Test
    public void shouldThrowNull_whenNumberOutOfRange() {
        assertNull(RomanNumberConverter.toRoman(0));
        assertNull(RomanNumberConverter.toRoman(123456789));
    }

}
