FROM openjdk:8
ADD target/number-conversion-spring-boot.war number-conversion-spring-boot.war
ENTRYPOINT ["java","-jar","number-conversion-spring-boot.war"]